const LIST_OF_DAYS = 'listOfDays';

window.onload = function () {
    setTimeout(() => window.scrollTo(0, 1), 100);
    loadCurrentDate();
};

function loadCurrentDate() {
    date.value = moment(new Date()).format('YYYY-MM-DD');
    loadCountersAndGoalByDate(date.value, false)
}

function showSection(section) {
    document.querySelectorAll('.section').forEach(s => s.classList.remove('show'));
    document.getElementById(`section${section}`).classList.add('show');
    document.querySelectorAll('.button-footer').forEach(s => s.classList.remove('selected'));
    document.getElementById(`button${section}`).classList.add('selected');

    if (section != 'Add') info.setAttribute('hidden', '');
    else info.removeAttribute('hidden');
    if (section == 'Days') loadList();
    if (section == 'Trend') loadChart();
}

function onDateChange() {
    dataNotFoundMessage.setAttribute('hidden', '');
    if (!date?.value) {
        loadCurrentDate();
        return;
    }

    loadCountersAndGoalByDate(date.value, true);
}

function loadCountersAndGoalByDate(selectedDate, showNotFoundMessage = false) {
    var days = getDays();
    var day = days?.find(d => d.date == selectedDate);
    if (!day) {
        if (showNotFoundMessage) dataNotFoundMessage.removeAttribute('hidden');
        dayCounter.value = '';
        nightCounter.value = '';
        return;
    }
    dayCounter.value = day.dayMaxPushups;
    nightCounter.value = day.nightMaxPushups;
    goalCounter.value = day.goal;
    highlightGoalIfReached(day);
}

function updateCountersAndGoal() {
    dataNotFoundMessage.setAttribute('hidden', '');
    var days = getDays();
    var currentDay = getCurrentDto();
    if (!days) {
        var newListOfDays = [currentDay];
        localStorage.setItem(LIST_OF_DAYS, JSON.stringify(newListOfDays));
        return;
    }
    var existingDay = days.find(d => d.date == date.value);
    if (existingDay) days = days.filter(d => d.date != date.value);
    if (dayCounter.value || nightCounter.value)
        days.push(currentDay);
    localStorage.setItem(LIST_OF_DAYS, JSON.stringify(days));
    highlightGoalIfReached(currentDay);
}

function highlightGoalIfReached(day) {
    if (+day.dayMaxPushups >= +goalCounter.value || +day.nightMaxPushups >= +goalCounter.value)
        bannerGoal.classList.add('highlight');
    else bannerGoal.classList.remove('highlight');
}

function loadList() {
    daysList.innerHTML = '';
    var days = getDays();
    if (!days) {
        dayHeader.setAttribute('hidden', '');
        buttonDeleteAll.setAttribute('hidden', '');
        return;
    }

    dayHeader.removeAttribute('hidden', '');
    buttonDeleteAll.removeAttribute('hidden', '');
    days.forEach(d => {
        var dayElement = createTrendElement(d);
        daysList.innerHTML += dayElement;
    });
}

function createTrendElement(day) {
    return `<div class="day-element ${day.goalWon ? 'star' : ''}"><span>${new Date(day.date).toLocaleDateString()}</span><span>${day.dayMaxPushups}</span><span>${day.nightMaxPushups}</span><span>${day.goal}</span></div>`;
}

function getCurrentDto() {
    return {
        date: date.value,
        dayMaxPushups:
            dayCounter.value,
        nightMaxPushups:
            nightCounter.value,
        goal: goalCounter.value,
        goalWon: +dayCounter.value >= +goalCounter.value || +nightCounter.value >= +goalCounter.value
    }
}

var chart = null;
function loadChart(type = 'bar') {
    var days = getDays();
    if (!days) return;

    const ctx = document.getElementById('chart');
    if (chart) chart.destroy();
    chart = new Chart(ctx, {
        type: type,
        data: {
            labels: days.map(d => new Date(d.date).toLocaleDateString()),
            datasets:
                [{
                    label: 'Max Pushups per day',
                    data: days.map(d => Math.max(d.dayMaxPushups, d.nightMaxPushups)),
                    fill: false,
                    borderColor: 'orange',
                    tension: 0.1,
                    color: 'white',
                    backgroundColor: '#FFFFFF'
                }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: { labels: { color: 'white' } }
            },
            scales: {
                y:{ ticks: { color: "white" } }, 
                x:{ ticks: { color: "white" } }
            }
        }
    });

    [...chartButtons.children].forEach(b => b.classList.remove('selected'));
    if (type == 'bar') buttonChartBar.classList.add('selected');
    if (type == 'line') buttonChartLine.classList.add('selected');
}

function getDays() {
    var daysAsString = localStorage.getItem(LIST_OF_DAYS);
    if (!daysAsString) return null;
    var days = JSON.parse(daysAsString)
    if (days) days = days.sort(function (a, b) { return new Date(a.date) - new Date(b.date) });
    return days;
}

// const hideMobileKeyboardOnReturn = (keyboardEvent) => {
//     element.addEventListener('keyup', (keyboardEvent) => {
//         if (keyboardEvent.code === 'Enter' || keyboardEvent.keyCode == 13 || keyboardEvent.keyCode == 9
//         || keyboardEvent.code == 13 || keyboardEvent.code == 9) {
//             element.blur();
//         }
//     });    

// };

// document.querySelementectorAll('input').forEach((element) => {
//     hideMobileKeyboardOnReturn(element);
// }); 

function hideKeyboard(event) {
    if (event.code != 'Enter' && event.keyCode != 13 && event.keyCode != 9)
        return;
    event.target.blur();
}

function askForClearAll() {
    if (!confirm("Delete all data?!")) return;
    localStorage.setItem(LIST_OF_DAYS, '');
    loadList();
}

function showSideMenu() {
    menu.classList.add('show');
}

function closeSideMenu() {
    menu.classList.remove('show');
}